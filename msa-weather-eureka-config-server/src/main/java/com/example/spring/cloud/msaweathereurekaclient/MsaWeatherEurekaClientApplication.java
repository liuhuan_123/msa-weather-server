package com.example.spring.cloud.msaweathereurekaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigServer
public class MsaWeatherEurekaClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsaWeatherEurekaClientApplication.class, args);
	}

}
