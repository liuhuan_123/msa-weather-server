package com.example.spring.cloud.msaweathereurekaclient.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Wrb
 * @date 2019/4/27 22:39
 */
@RestController
public class HelloController {

	@GetMapping("/hello")
	public String hello() {
		return "hello world!";
	}
}