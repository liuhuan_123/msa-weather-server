package com.example.spring.cloud.weather.service;

/**
 * @author Wrb
 * @date 2019/4/27 22:27
 */
public interface WeatherDataCollectionService {

	/**
	 * 根据城市ID同步天气
	 * @param cityId
	 */
	void syncDateByCityId(String cityId);
}
