package com.example.spring.cloud.weather.service;

import com.example.spring.cloud.weather.vo.Forecast;
import com.example.spring.cloud.weather.vo.Weather;
import com.example.spring.cloud.weather.vo.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Wrb
 * @date 2019/4/24 23:19
 */
@Service
public class WeatherReportServiceImpl implements WeatherReportService {

	@Autowired
	private DataClient dataClient;

	@Override
	public Weather getDataByCityId(String cityId) {
		//由天气数据API微服务来提供
		WeatherResponse response = dataClient.getDataByCityId(cityId);
		Weather data = null;
		if (response != null) {
			data = response.getData();
		}
		return data;
	}
}
