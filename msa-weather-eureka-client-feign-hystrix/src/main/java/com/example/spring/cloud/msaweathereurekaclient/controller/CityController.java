package com.example.spring.cloud.msaweathereurekaclient.controller;

import com.example.spring.cloud.msaweathereurekaclient.service.CityClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Wrb
 * @date 2019/5/14 18:13
 */
@RestController
public class CityController {

	@Autowired
	private CityClient cityClient;

	@GetMapping("/cities")
	@HystrixCommand(fallbackMethod = "defaultCities")
	public String listCity() {
		String body = cityClient.listCity();
		return body;
	}

	public String defaultCities() {
		return "city Data Server is down!";
	}
}
