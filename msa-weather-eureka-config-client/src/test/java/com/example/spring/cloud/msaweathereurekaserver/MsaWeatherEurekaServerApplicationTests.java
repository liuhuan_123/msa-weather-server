package com.example.spring.cloud.msaweathereurekaserver;

import com.example.spring.cloud.msaweathereurekaclient.MsaWeatherEurekaClientApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MsaWeatherEurekaClientApplication.class)
public class MsaWeatherEurekaServerApplicationTests {

	@Value("${auther}")
	private String auther;

	@Test
	public void contextLoads() {
		System.out.println(auther.equals("bao"));
	}

}
