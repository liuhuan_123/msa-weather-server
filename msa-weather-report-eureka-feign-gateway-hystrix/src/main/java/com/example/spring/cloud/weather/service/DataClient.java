package com.example.spring.cloud.weather.service;

import com.example.spring.cloud.weather.vo.City;
import com.example.spring.cloud.weather.vo.WeatherResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author Wrb
 * @date 2019/5/16 10:57
 */
@FeignClient(value = "msa-weather-eureka-client-zuul",fallback = DataClientFallback.class)
public interface DataClient {

	@GetMapping("/city/cities")
	List<City> listCity() throws Exception;

	@GetMapping("/data/weather/cityId/{cityId}")
	WeatherResponse getDataByCityId(@PathVariable("cityId") String cityId);
}
