package com.example.spring.cloud.weather.service;

import com.example.spring.cloud.weather.vo.Weather;

/**
 * @author Wrb
 * @date 2019/4/24 23:18
 */
public interface WeatherReportService {

	/**
	 * 根据城市ID查询天气信息
	 * @param cityId
	 * @return
	 */
	Weather getDataByCityId(String cityId);
}
