package com.example.spring.cloud.weather.service;

import com.example.spring.cloud.weather.vo.City;
import com.example.spring.cloud.weather.vo.WeatherResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Wrb
 * @date 2019/5/16 16:29
 */
@Component
public class DataClientFallback implements DataClient{

	@Override
	public List<City> listCity() throws Exception {
		List<City> cityList = new ArrayList<>();

		City city = new City();
		city.setCityId("101280601");
		city.setCityName("深圳");
		cityList.add(city);

		city = new City();
		city.setCityId("101280301");
		city.setCityName("惠州");
		cityList.add(city);

		return cityList;
	}

	@Override
	public WeatherResponse getDataByCityId(String cityId) {
		return null;
	}
}
