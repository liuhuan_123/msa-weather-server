package com.demo.spring.cloud.weather.service;

import com.demo.spring.cloud.weather.vo.WeatherResponse;

/**
 * @author WangRuBao
 * @date 2018/8/10 22:28
 */
public interface WeatherDataService {

	/**
	 * 根据城市ID查询天气数据
	 * @param cityId
	 * @return
	 */
	WeatherResponse getDataByCityId(String cityId);

	/**
	 * 根据城市名称查询天气数据
	 * @param cityName
	 * @return
	 */
	WeatherResponse getDataByCityName(String cityName);
}
